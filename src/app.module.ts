import { Module } from '@nestjs/common';
import { ApiModule } from './modlues/api/api.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env'
    }),
    ApiModule
  ],
  providers: [],
})
export class AppModule {}
