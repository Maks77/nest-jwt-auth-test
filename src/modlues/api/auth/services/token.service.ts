import { Injectable } from '@nestjs/common';
import { UserEntity } from '../../../core/user/entity/user.entity';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class TokenService {

  constructor(
    private readonly jwt: JwtService
  ) {}

  public async generateAccessToken(user: UserEntity): Promise<string> {
    const options = { subject: String(user.id) }
    return this.jwt.signAsync({}, options)
  }

}
