import { Inject, Injectable, NotFoundException, UnprocessableEntityException } from '@nestjs/common';
import { RegisterUserRequestDto } from '../dto/register-user-request.dto';
import { IUserRepository } from '../../../core/user/repository/user-repository.interface';
import { ResetPasswordRequestDto } from '../dto/reset-password-request.dto';
import { ForgotPasswordRequestDto } from '../dto/forgot-password-request.dto';
import { LoginUserRequestDto } from '../dto/login-user-request.dto';
import { TokenService } from './token.service';
import { AuthResponseDto } from '../dto/auth-response.dto';
import { IMailer } from '../../../infrastructure/mailer/model/mailer.interface';
import { UserEntity } from '../../../core/user/entity/user.entity';
import { ConfigService } from '@nestjs/config';
import { ForgotPasswordResponseDto } from '../dto/forgot-password-response.dto';

@Injectable()
export class AuthService {

  constructor(
    @Inject('USER_REPOSITORY')
    private readonly userRepository: IUserRepository,
    @Inject('MAILER')
    private readonly mailer: IMailer,
    private readonly tokenService: TokenService,
    private readonly configService: ConfigService
  ) {}

  public async registerUser(registerUserDto: RegisterUserRequestDto): Promise<AuthResponseDto> {
    const existedUser = await this.userRepository.findByEmail(registerUserDto.email)
    if (existedUser) {
      throw new UnprocessableEntityException('User with such email is already exists')
    }
    try {
      const user = new UserEntity()
      user.id = Date.now().toString()
      user.password = registerUserDto.password
      user.email = registerUserDto.email
      await this.userRepository.save(user)
      const token = await this.tokenService.generateAccessToken(user)

      return { access_token: token }
    } catch (e) {
      throw new UnprocessableEntityException()
    }
  }

  public async loginUser(loginUserDto: LoginUserRequestDto): Promise<AuthResponseDto> {
    const existedUser = await this.userRepository.findByEmail(loginUserDto.email)
    if (!existedUser) {
      throw new NotFoundException('User not found')
    }

    if (existedUser.password !== loginUserDto.password) {
      throw new UnprocessableEntityException('Incorrect password')
    }

    const token = await this.tokenService.generateAccessToken(existedUser)
    return { access_token: token }
  }

  public async forgotPassword(
    forgotPasswordDto: ForgotPasswordRequestDto
  ): Promise<ForgotPasswordResponseDto> {
    const existedUser = await this.userRepository.findByEmail(forgotPasswordDto.email)
    if (!existedUser) {
      throw new NotFoundException('User not found')
    }
    try {
      const token = await this.tokenService.generateAccessToken(existedUser)
      await this.mailer.send({
        email: forgotPasswordDto.email,
        text: `Token: ${token}`,
        subject: 'Forgot password'
      })
      const isLocalEnv = this.configService.get<string>('ENVIRONMENT') === 'local'
      return { status: 'success', token: isLocalEnv ? token : '' }
    } catch (e) {
      throw new UnprocessableEntityException('Fail during mail sending')
    }
  }

  public async resetPassword(
    user: UserEntity,
    resetPasswordDto: ResetPasswordRequestDto
  ): Promise<AuthResponseDto> {
    user.password = resetPasswordDto.new_password
    await this.userRepository.save(user)
    const token = await this.tokenService.generateAccessToken(user)
    return { access_token: token }
  }

}
