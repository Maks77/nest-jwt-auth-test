import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from "passport-jwt";
import { ConfigService } from '@nestjs/config';
import { AccessTokenPayload } from '../models/access-token-payload.model';
import { IUserRepository } from '../../../core/user/repository/user-repository.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy){

  constructor(
    private readonly configService: ConfigService,
    @Inject('USER_REPOSITORY') private readonly userRepository: IUserRepository
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('JWT_SECRET'),
      signOptions: {
        expiresId: configService.get<string>("JWT_EXPIRES_IN")
      }
    })
  }

  async validate(payload: AccessTokenPayload) {
    const {sub: id} = payload

    const existedUser = await this.userRepository.findById(id)

    if (!existedUser) {
      throw new UnauthorizedException()
    }

    return existedUser
  }

}
