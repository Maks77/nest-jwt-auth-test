import { Request } from 'express'
import { UserEntity } from '../../../core/user/entity/user.entity';
export type AuthRequest = Request & { user: UserEntity}
