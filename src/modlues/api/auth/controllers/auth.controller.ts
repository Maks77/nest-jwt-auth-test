import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { RegisterUserRequestDto } from '../dto/register-user-request.dto';
import { AuthService } from '../services/auth.service';
import { LoginUserRequestDto } from '../dto/login-user-request.dto';
import { ForgotPasswordRequestDto } from '../dto/forgot-password-request.dto';
import { ResetPasswordRequestDto } from '../dto/reset-password-request.dto';
import { JwtGuard } from '../guards/jwt.guard';
import { AuthRequest } from '../models/auth-request.model';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthResponseDto } from '../dto/auth-response.dto';
import { ForgotPasswordResponseDto } from '../dto/forgot-password-response.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {

  constructor(
    private readonly authService: AuthService
  ) {}

  @Get('test')
  @UseGuards(JwtGuard)
  @ApiBearerAuth('JWT')
  @ApiOperation({summary: 'Endpoint to test work of JwtGuard'})
  async test(
    @Req() req: AuthRequest
  ) {
    return { user: req.user }
  }

  @Post('register')
  @ApiOkResponse({type: AuthResponseDto})
  public async register(
    @Body() body: RegisterUserRequestDto
  ) {
    return this.authService.registerUser(body)
  }

  @Post('login')
  @ApiOkResponse({type: AuthResponseDto})
  public async login(
    @Body() body: LoginUserRequestDto
  ) {
    return this.authService.loginUser(body)
  }

  @Post('forgot-password')
  @ApiOkResponse({type: ForgotPasswordResponseDto})
  @ApiOperation({
    description: '[token] filed is returned only with "local" environment for logic testing'
  })
  public async forgotPassword(
    @Body() body: ForgotPasswordRequestDto
  ) {
    return this.authService.forgotPassword(body)
  }

  @Post('reset-password')
  @UseGuards(JwtGuard)
  @ApiBearerAuth('JWT')
  @ApiOkResponse({type: AuthResponseDto})
  public async resetPassword(
    @Req() req: AuthRequest,
    @Body() body: ResetPasswordRequestDto
  ) {
    return this.authService.resetPassword(req.user, body)
  }

}
