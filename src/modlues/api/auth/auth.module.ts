import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthController } from './controllers/auth.controller';
import { AuthService } from './services/auth.service';
import { UserModule } from '../../core/user/user.module';
import { UserRepositoryLocalAdapter } from '../../core/user/repository/user-repository.local-adapter';
import { JwtStrategy } from './strategies/jwt.strategy';
import { TokenService } from './services/token.service';
import { MailerModule } from '../../infrastructure/mailer/mailer.module';
import { LocalMailerAdapter } from '../../infrastructure/mailer/services/local-mailer.adapter';

@Module({
  imports: [
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
        signOptions: { expiresIn: configService.get<string>('JWT_EXPIRES_IN')}
      })
    }),
    PassportModule,
    UserModule,
    ConfigModule,
    MailerModule
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    JwtStrategy,
    TokenService,
    {
      provide: 'USER_REPOSITORY',
      useClass: UserRepositoryLocalAdapter
    },
    {
      provide: 'MAILER',
      useClass: LocalMailerAdapter
    }
  ]
})
export class AuthModule {}
