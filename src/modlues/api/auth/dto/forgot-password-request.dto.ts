import { ApiProperty } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';

export class ForgotPasswordRequestDto {

  @ApiProperty({type: String, required: true})
  @IsEmail()
  email: string

}
