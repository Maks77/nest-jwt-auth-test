import { ApiProperty } from '@nestjs/swagger';
import { IsString, MaxLength, MinLength } from 'class-validator';

export class ResetPasswordRequestDto {

  @ApiProperty({ type: String, required: true })
  @IsString()
  @MinLength(3)
  @MaxLength(16)
  new_password: string
}
