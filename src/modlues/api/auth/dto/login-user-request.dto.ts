import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MaxLength, MinLength } from 'class-validator';

export class LoginUserRequestDto {
  @ApiProperty({type: String, required: true})
  @IsEmail()
  email: string

  @ApiProperty({ type: String, required: true })
  @IsString()
  @MinLength(3)
  @MaxLength(16)
  password: string
}
