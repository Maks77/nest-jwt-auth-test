import { ApiProperty } from '@nestjs/swagger';

export class AuthResponseDto {
  @ApiProperty({type: String})
  access_token: string
}
