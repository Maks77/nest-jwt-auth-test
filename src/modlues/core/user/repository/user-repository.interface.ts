import { UserEntity } from '../entity/user.entity';

export interface IUserRepository {

  findById(id: string): Promise<UserEntity | undefined>

  findByEmail(email: string): Promise<UserEntity | undefined>

  save(user: UserEntity): Promise<void>

}
