import { Injectable } from '@nestjs/common';
import { IUserRepository } from './user-repository.interface';
import { UserEntity } from '../entity/user.entity';

/**
 * Local implementation of repository (e.g. TypeORM Repository)
 * */
@Injectable()
export class UserRepositoryLocalAdapter implements IUserRepository {
  users: UserEntity[] = [
    {id: Date.now().toString(), email: 'm@mail.com', password: 'somepassword'}
  ]

  public async findById(id: string): Promise<UserEntity | undefined> {
    return this.users.find(user => user.id === id);
  }

  public async findByEmail(email: string): Promise<UserEntity | undefined> {
    return this.users.find(user => user.email === email);
  }

  public async save(user: UserEntity): Promise<void> {
    let existed = this.users.find(el => el.email === user.email)
    if (existed) {
      existed = user
      return
    }
    this.users.push(user);
  }

}
