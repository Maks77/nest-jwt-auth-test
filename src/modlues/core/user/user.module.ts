import { Module } from '@nestjs/common';
import { UserRepositoryLocalAdapter } from './repository/user-repository.local-adapter';

@Module({
  providers: [UserRepositoryLocalAdapter],
  exports: [UserRepositoryLocalAdapter]
})
export class UserModule {}
