export interface SendMailOptions {
  email: string
  subject: string
  text: string
}
