import { SendMailOptions } from './send-mail-options.model';

export interface IMailer {
  send(options: SendMailOptions): Promise<void>
}

