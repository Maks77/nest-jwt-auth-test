import { Module } from '@nestjs/common';
import { LocalMailerAdapter } from './services/local-mailer.adapter';

@Module({
  providers: [LocalMailerAdapter],
  exports: [LocalMailerAdapter],
})
export class MailerModule {}
