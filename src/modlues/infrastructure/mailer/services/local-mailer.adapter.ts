import { Injectable } from '@nestjs/common';
import { IMailer} from '../model/mailer.interface';
import { SendMailOptions } from '../model/send-mail-options.model';

/**
 * Local adapter emulates sending email logic
 * */
@Injectable()
export class LocalMailerAdapter implements IMailer {

  public async send(options: SendMailOptions): Promise<void> {
    // Send mail implementation...
  }

}
